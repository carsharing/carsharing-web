import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {api} from '../constants/api';
import {BaseService} from './base.service';
import {keys} from '../constants/storage.keys';
import {Signup} from '../models';


@Injectable()
export class AuthService extends BaseService {
  private static SERVER_URL = api.SERVER_URL;
  private static USER_LOGIN = api.USER_LOGIN;
  private static USER_SIGNUP = api.USER_SIGNUP;

  constructor(private http: HttpClient, private router: Router) {
    super();
  }

  login(params: any): Observable<any> {
    const headers = this.getBasicJsonHeader();
    const userParams = JSON.stringify(params);
    return this.http.post(AuthService.USER_LOGIN, userParams, {
      headers: headers
    });
  }

  signup(params: Signup) {
    const headers = this.getBasicJsonHeader();
    const userData = JSON.stringify(params);
    return this.http.post(AuthService.USER_SIGNUP, userData, {
      headers: headers
    });
  }

  logout(): void {
    localStorage.removeItem(keys.USER);
    localStorage.removeItem(keys.TOKEN);
    this.router.navigate(['/']);
  }

}
