import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {api} from '../constants/api';
import {BaseService} from './base.service';
import {Router} from '@angular/router';
import {keys} from '../constants/storage.keys';
import {Passport} from '../models';

@Injectable()
export class PassportService extends BaseService{
  private static SERVER_URL = api.SERVER_URL;
  private static USER_GET_PASSPORT = api.USER_GET_PASSPORT;

  constructor(private http: HttpClient,private router: Router) {
    super();
  }

  getById(id: string): Observable<any> {
    const headers = this.getBearerHeaders();
    return this.http.get(PassportService.USER_GET_PASSPORT+id, {
      headers: headers
    })
  }
}
