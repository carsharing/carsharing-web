import { Injectable } from '@angular/core';
import { Router, NavigationStart } from '@angular/router';
import { Subject ,  Observable } from 'rxjs';
import { Alert, AlertType } from '../models/alert';

@Injectable()
export class AlertService {
  private subject = new Subject<Alert>();
  private keepAfterRouteChange = false;

  constructor(private router: Router) {
    // clear alert messages on route change unless 'keepAfterRouteChange' flag is true
    router.events.subscribe(event => {
        if (event instanceof NavigationStart) {
            if (this.keepAfterRouteChange) {
                // only keep for a single route change
                this.keepAfterRouteChange = false;
            } else {
                // clear alert messages
                this.clear();
            }
        }
    });
  }

   getAlert(): Observable<any> {
       return this.subject.asObservable();
   }

   success(message: string, keepAfterRouteChange = true) {
     console.log(message);
       this.alert(AlertType.Success, message, keepAfterRouteChange);
   }

  error(message: any, keepAfterRouteChange = false) {
    let err = message.error.errors;
    this.alert(AlertType.Error, message.error.message, keepAfterRouteChange);
    console.log(message.error.message);
    if (err.password && err.username ) {
      this.alert(AlertType.Error, 'username: '+err.username+';\n'+'password: '+err.password, keepAfterRouteChange);
    } else if(err.password) {
      this.alert(AlertType.Error, err.password, keepAfterRouteChange);
    } else if(err.username) {
      this.alert(AlertType.Error, err.username, keepAfterRouteChange);
    } else if(message.error.errors) {
      this.alert(AlertType.Error, message.error.errors, keepAfterRouteChange);
    }
    // else if (message.error.message){
    // }
  }

   info(message: string, keepAfterRouteChange = false) {
       this.alert(AlertType.Info, message, keepAfterRouteChange);
   }

   warn(message: string, keepAfterRouteChange = false) {
       this.alert(AlertType.Warning, message, keepAfterRouteChange);
   }

   alert(type: AlertType, message: string, keepAfterRouteChange = false) {
       this.keepAfterRouteChange = keepAfterRouteChange;
       this.subject.next(<Alert>{ type: type, message: message });
   }

  clear() {
         // clear alerts
         this.subject.next();
     }
}
