import { Injectable } from '@angular/core';
import {BaseService} from './base.service';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {api} from '../constants/api';

@Injectable()
export class CarService extends BaseService{
  private static SERVER_URL = api.SERVER_URL;
  private static ADMIN_ADD_CAR = api.ADMIN_ADD_CAR;
  private static ADMIN_CAR_LIST = api.ADMIN_CAR_LIST;
  private static ADMIN_REMOVE_CAR = api.ADMIN_REMOVE_CAR;
  private static ADMIN_GET_CAR = api.ADMIN_GET_CAR;

  constructor(private http: HttpClient, private router: Router) {
    super();
  }

  addingCar(params: any): Observable<any> {
    const headers = this.getBearerHeaders();
    const carParams = JSON.stringify(params);
    return this.http.post(CarService.ADMIN_ADD_CAR, carParams, {
      headers: headers
    });
  }

  getCarsList(): Observable<any> {
    const headers = this.getBearerHeaders();
    console.log(CarService.SERVER_URL+CarService.ADMIN_CAR_LIST);
    return this.http.get(CarService.ADMIN_CAR_LIST, {
      headers: headers
    })
  }

  getCarById(carId): Observable<any> {
    console.log(carId);
    const headers = this.getBearerHeaders();
    return this.http.get(CarService.ADMIN_GET_CAR+carId, {
      headers: headers
    })
  }

  removeCar(): Observable<any> {
    const headers = this.getBearerHeaders();
    return this.http.delete(CarService.ADMIN_REMOVE_CAR, {
      headers: headers
    })
  }
}
