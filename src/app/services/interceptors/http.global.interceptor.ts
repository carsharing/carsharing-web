import {Injectable} from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import {Observable} from 'rxjs/Rx';
import {api} from '../../constants/api';

@Injectable()
export class HttpGlobalInterceptor implements HttpInterceptor {
  constructor() {
  }

  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    if (!request.url.includes('assets')) {
      const newParams = request.params;
      const newHeaders = request.headers;

      const newUrl = api.SERVER_URL + request.url;
      request = request.clone({ headers: newHeaders, params: newParams, url: newUrl });
    }
    return next.handle(request);
  }
}
