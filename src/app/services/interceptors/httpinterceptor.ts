
import {throwError as observableThrowError,  Observable } from 'rxjs';
import { Injectable, Injector } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor,
  HttpErrorResponse,
  HttpResponse
} from '@angular/common/http';
import {Router} from '@angular/router';
import {api} from '../../constants/api';
import 'rxjs/add/operator/do';
import { map, filter, tap } from 'rxjs/operators';



import { AlertService } from '../alert.service';

@Injectable()
export class MyHttpInterceptor implements HttpInterceptor {
  constructor(private router: Router, private alertService: AlertService) {}

  intercept(req: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {

    //send the newly created request
    return next.handle(req)
      .pipe(
        tap(event => {
          if (event instanceof HttpResponse) {
            if (event.url === api.SERVER_URL+'/api/users/login') {
              this.alertService.success('you successfully logged in!');
            }
          }
        }, error => {
          // intercept the respons error and displace it to the console
          console.log(error);
            this.alertService.error(error);
          console.log("Error Occurred");
          //return the error to the method that called it
          return observableThrowError(error);
        })
      )

      //Commented after migrating from Angular 5.0 to 6.0.9
      // .do((event: HttpEvent<any>) => {
      //   console.log(event);
      //   if (event instanceof HttpResponse) {
      //     if (event.url === api.SERVER_URL+'/api/users/login') {
      //       this.alertService.success('you successfully logged in!');
      //     }
      //   }
      // })
      // .catch((error, caught) => {
      //   //intercept the respons error and displace it to the console
      //   console.log(error);
      //     this.alertService.error(error);
      //   console.log("Error Occurred");
      //   //return the error to the method that called it
      //   return observableThrowError(error);
      // }) as any;
  }
}
