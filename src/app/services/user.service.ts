import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {Router} from '@angular/router';
import {api} from '../constants/api';
import {BaseService} from './base.service';
import {keys} from '../constants/storage.keys';

@Injectable()
export class UserService extends BaseService{
  private static SERVER_URL = api.SERVER_URL;
  private static USER_ADD_PASSPORT = api.USER_ADD_PASSPORT;
  private static USER_GET_LIST = api.USER_GET_LIST;
  private static USER_GET_USER = api.USER_GET_USER;
  private static USER_EDIT_PROFILE = api.USER_EDIT_PROFILE;

  constructor(private http: HttpClient, private router: Router) {
    super();
  }

  addPassport(params: any): Observable<any> {
    const headers = this.getBearerHeaders();
    const passportParams = JSON.stringify(params);
    return this.http.post(UserService.USER_ADD_PASSPORT, passportParams, {
      headers: headers
    });
  }

  editProfile(params: any): Observable<any> {
    const headers = this.getBearerHeaders();
    const profileParams = JSON.stringify(params);
    return this.http.post(UserService.USER_EDIT_PROFILE, profileParams, {
      headers: headers
    });
  }

  getUsersList(): Observable<any> {
    const headers = this.getBearerHeaders();
    return this.http.get(UserService.USER_GET_LIST, {
      headers: headers
    })
  }

  getById(id: string): Observable<any> {
    const headers = this.getBearerHeaders();
    return this.http.get(UserService.USER_GET_USER+id, {
      headers: headers
    })
  }

  getUserInfoFromToken() {
    return JSON.parse(localStorage.getItem('USER')).user.user;
  }
}
