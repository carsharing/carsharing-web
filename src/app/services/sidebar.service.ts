import { Injectable } from '@angular/core';
import * as $ from 'jquery';

@Injectable()
export class SidebarService {

  isExpanded: boolean;

  constructor() {
    this.isExpanded = true;
  }

  toggleSidebar() {
    this.isExpanded = !this.isExpanded;
    $(".dashboard-container").toggleClass("toggled");
    return this.isExpanded;
  }
}
