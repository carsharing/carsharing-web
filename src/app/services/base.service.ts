import {Injectable} from '@angular/core';
import {keys} from '../constants/storage.keys';
import {HttpHeaders, HttpParams} from '@angular/common/http';

@Injectable()
export abstract class BaseService {

  private static CLIENT_ID = 'client_id';

  constructor() {
  }

  protected getBearerHeaders(): HttpHeaders {
    const token = JSON.parse(localStorage.getItem('TOKEN')).token;
    const headers = {
      'Content-Type': 'application/json',
      'Authorization': 'Bearer ' + token
    };
    return new HttpHeaders(headers);
  }

  protected getBasicParams(username: string, password: string): HttpParams {
    return new HttpParams()
      .set('username', username)
      .set('password', password);
  }

  protected getBasicJsonHeader(): HttpHeaders {
    const headers = {
      'Accept': 'application/json',
      'Content-Type': 'application/json'
    };
    return new HttpHeaders(headers);
  }
}
