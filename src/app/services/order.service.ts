import { Injectable } from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {Observable} from 'rxjs';
import {api} from '../constants/api';
import {BaseService} from './base.service';
import {Router} from '@angular/router';
import {keys} from '../constants/storage.keys';
import {Order} from '../models';

@Injectable()
export class OrderService extends BaseService{
  private static ORDER_CREATE = api.ORDER_CREATE;
  private static ORDER_LIST = api.ORDER_LIST;

  constructor(private http: HttpClient,private router: Router) {
    super();
  }

  addingOrder(userId: string, carId: string): Observable<any> {
    const headers = this.getBearerHeaders();
    let params = {
      userId: userId,
      carId: carId
    };
    const carParams = JSON.stringify(params);
    return this.http.post(OrderService.ORDER_CREATE+'/user/'+userId+'/car/'+carId, carParams, {
      headers: headers
    })
  }

  list(): Observable<any> {
    const headers = this.getBearerHeaders();

    return this.http.get(OrderService.ORDER_LIST, {
      headers: headers
    })
  }
}
