import {keys} from '../constants/storage.keys';
import {User} from '../models/user';

export class UserAuth {

  public static isAdmin(): boolean {
    return UserAuth.has('ROLE_ADMIN');
  }

  public static isUser(): boolean {
    return UserAuth.has('ROLE_USER');
  }

  static has(role: string): boolean {
    const user: User = this.getCurrentUser();
    if (user != null) {
      if (user.role === role) return true;
    }
    return false;
  }

  public static getCurrentUser(): User {
    return JSON.parse(localStorage.getItem(keys.USER));
  }

}
