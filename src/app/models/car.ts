export class Car {

  mark: string;
  model: string;
  bodyType: string;
  engineVolume: number;
  gearBoxType: string;
  fuelType: string;
  year: number;
  priceCategory: number;
  registrationPlate: string;
  sideNumber: number;
  mileage: number;

  constructor(mark: string, model: string, bodyType: string,
      engineVolume: number, gearBoxType: string, fuelType: string,
      year: number, priceCategory: number, registrationPlate: string,
      sideNumber: number, mileage: number) {
    this.mark = mark;
    this.model = model;
    this.bodyType = bodyType;
    this.engineVolume = engineVolume;
    this.gearBoxType = gearBoxType;
    this.fuelType = fuelType;
    this.year = year;
    this.priceCategory = priceCategory;
    this.registrationPlate = registrationPlate;
    this.sideNumber = sideNumber;
    this.mileage = mileage;
  }

}
