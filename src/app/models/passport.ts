export class Passport {

  firstName: string;
  lastName: string;
  dateOfBirth: string;
  identificationNumber: string;
  passportNumber: string;
  dateOfExpiry: string;


  constructor(
    lastName: string,
    firstName: string,
    dateOfBirth: string,
    identificationNumber: string,
    passportNumber: string,
    dateOfExpiry: string) {
    this.lastName = lastName;
    this.firstName = firstName;
    this.identificationNumber = identificationNumber;
    this.passportNumber = passportNumber;
    this.dateOfBirth = dateOfBirth;
    this.dateOfExpiry = dateOfExpiry;
  }

}
