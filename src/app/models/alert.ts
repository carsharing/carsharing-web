export class Alert {
    type: AlertType;
    message: any;
}

export enum AlertType {
    Success,
    Error,
    Info,
    Warning
}
