export class Order {

  carId: string;
  userId: string;

  constructor(carId: string, userId: string) {
    this.carId = carId;
    this.userId = userId;
  }
}
