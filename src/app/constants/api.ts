export const api = {
  SERVER_URL: 'http://188.166.77.17:8200',
//   SERVER_URL: 'http://localhost:8000',
  USER_LOGIN: '/api/users/login',
  USER_SIGNUP: '/api/users/signup',
  USER_ADD_PASSPORT: '/api/users/passport',
  USER_EDIT_PROFILE: '/api/users/edit',
  USER_GET_LIST: '/api/users/list',
  USER_GET_USER: '/api/users/',
  ADMIN_ADD_CAR: '/api/cars/create',
  ADMIN_CAR_LIST: '/api/cars/list',
  ADMIN_REMOVE_CAR: '/api/cars/remove',
  ADMIN_GET_CAR: '/api/cars/',

  USER_GET_PASSPORT: '/api/users/passport/',
  ORDER_CREATE: '/api/orders/create',
  ORDER_LIST: '/api/orders/list',
};
