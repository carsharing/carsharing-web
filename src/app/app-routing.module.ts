import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

import { AuthLoginComponent } from './components/auth/auth-login/auth-login.component';
import { AuthSignUpComponent } from './components/auth/auth-signup/auth-signup.component';
import { MainComponent } from './components/main/main.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';

// import { AuthUserDataComponent } from './components/auth/auth-user-data/auth-user-data.component';
// import { ProfilePageComponent } from './components/profile-page/profile-page.component';

const routes: Routes = [
  { path: '', component: MainComponent },
  { path: 'login', component: AuthLoginComponent },
  { path: 'signup', component: AuthSignUpComponent },
  { path: '**', component: NotFoundPageComponent },
];

@NgModule({
  imports: [ RouterModule.forRoot(routes, {
    onSameUrlNavigation: 'ignore'
  }) ],
  exports: [ RouterModule ]
})
export class AppRoutingModule { }
