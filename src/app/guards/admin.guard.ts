import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Observable } from 'rxjs';
import {UserAuth} from '../helpers/user.auth';

@Injectable()
export class AdminGuard implements CanActivate {
  
  canActivate(): boolean {
    return  UserAuth.isAdmin();
  }
}
