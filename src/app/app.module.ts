import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from './shared/shared.module';
import {Router} from '@angular/router';

import { HttpClientModule, HttpClient, HTTP_INTERCEPTORS } from '@angular/common/http';
import { TranslateModule, TranslateLoader } from '@ngx-translate/core';
import { TranslateHttpLoader } from '@ngx-translate/http-loader';
import { MyHttpInterceptor } from './services/interceptors/httpinterceptor'
import {HttpGlobalInterceptor} from './services/interceptors/http.global.interceptor';

import { NgbModule } from '@ng-bootstrap/ng-bootstrap';

import { AppComponent } from './app.component';
import { NavbarComponent } from './components/navbar/navbar.component';
import { PriceComponent } from './components/price/price.component';
import { AuthSignUpComponent } from './components/auth/auth-signup/auth-signup.component';
import { AuthLoginComponent } from './components/auth/auth-login/auth-login.component';
import { MainComponent } from './components/main/main.component';
import { AuthUserDataModule } from './components/auth/auth-user-data/auth-user-data.module';
import { LoadingModule } from './components/common/loading/loading.module';
import { ProfilePageComponent } from './components/profile-page/profile-page.component';

import { AppRoutingModule } from './app-routing.module';
import { FooterComponent } from './components/main/footer/footer.component';
import { PriceCardComponent } from './components/main/price-card/price-card.component';
import { NotFoundPageComponent } from './components/not-found-page/not-found-page.component';

// import { AdminModule } from './components/admin/admin.module';
import { DataTableModule } from 'angular5-data-table';
// import { AdminCarDetailsComponent } from './components/admin/admin-car-details/admin-car-details.component';
import { DashboardModule } from './components/dashboard/dashboard.module';
import { SupportService } from './services/support.service';
import { UserService } from './services/user.service';
import { CarService } from './services/car.service';
import { AlertService } from './services/alert.service';
import { PassportService } from './services/passport.service';
import { AlertComponent } from './components/common/alert/alert.component';
import { MapService } from './services/map.service';
import { StatisticService } from './services/statistic.service';
import { OrderService } from './services/order.service';


@NgModule({
  declarations: [
    AppComponent,
    NavbarComponent,
    PriceComponent,
    AuthSignUpComponent,
    AuthLoginComponent,
    MainComponent,
    ProfilePageComponent,
    FooterComponent,
    PriceCardComponent,
    NotFoundPageComponent,
    AlertComponent,
    // AdminCarDetailsComponent
  ],
  imports: [
    BrowserModule,
    NgbModule.forRoot(),
    HttpClientModule,
    LoadingModule,
    FormsModule,
    SharedModule,
    // AuthUserDataModule,
    DataTableModule,
    // AdminModule,
    DashboardModule,
    AppRoutingModule,
    TranslateModule.forRoot({
      loader: {
        provide: TranslateLoader,
        useFactory: HttpLoaderFactory,
        deps: [HttpClient]
      }
    })
  ],
  providers: [
    SupportService,
    UserService,
    OrderService,
    CarService,
    PassportService,
    AlertService,
    MapService,
    StatisticService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: HttpGlobalInterceptor,
      multi: true
    },
    {
      provide: HTTP_INTERCEPTORS,
      useClass: MyHttpInterceptor,
      multi: true
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule {}

export function HttpLoaderFactory(http: HttpClient) {
  return new TranslateHttpLoader(http);
}
