import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AuthUserDataComponent } from './auth-user-data.component';

describe('AuthUserDataComponent', () => {
  let component: AuthUserDataComponent;
  let fixture: ComponentFixture<AuthUserDataComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AuthUserDataComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AuthUserDataComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
