import { Component, OnInit } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import {Router} from '@angular/router';

import {UserService} from '../../../services/user.service';
import {Passport} from '../../../models';


@Component({
  selector: 'app-auth-user-data',
  templateUrl: './auth-user-data.component.html',
  styleUrls: ['./auth-user-data.component.scss'],
  providers: [UserService]
})
export class AuthUserDataComponent implements OnInit {

  passport : Passport;
  driverLicenseId: string;
  driverLicenseDateTo: string;

  selectedFiles = null;
  error = '';
  params = {};
  loading = false;

  constructor(private router: Router, private userService: UserService) { }

  ngOnInit() {
    this.passport = new Passport('', '', '', '', '', '');
  }

  addPassport() {
    this.loading = true;
    this.error = '';
    console.log(this.passport)
    this.userService.addPassport(this.passport)
      .subscribe(data => {
        console.log(data);
        this.router.navigate(['user/profile'])
        this.loading = false;
        },
        error => {
          this.error = 'Passport error';
          this.loading = false;
        });
  }

  onFileSelected(e) {
    this.selectedFiles = e.target.files;
  }

  onUploadPhoto() {
  }
}
