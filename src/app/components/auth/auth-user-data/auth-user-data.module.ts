import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '../../../shared/shared.module';

import {AuthUserDataComponent} from './auth-user-data.component';


@NgModule({
  imports: [
    CommonModule,
    HttpClientModule,
    FormsModule,
    SharedModule
  ],
  declarations: [
    AuthUserDataComponent
  ]
})
export class AuthUserDataModule { }
