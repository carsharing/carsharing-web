import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {keys} from '../../../constants/storage.keys';
import {Signup} from '../../../models';

@Component({
  selector: 'app-auth-signup',
  templateUrl: './auth-signup.component.html',
  styleUrls: ['./auth-signup.component.scss'],
  providers: [AuthService]
})
export class AuthSignUpComponent implements OnInit {

  password2 : string;
  user : Signup;
  error = '';
  loading = false;

  constructor(private router: Router, private authService: AuthService) { }


  ngOnInit() {
    this.user = new Signup('', '', '');
  }

  signup () {
    this.loading = true;
    this.error = '';

    this.authService.signup(this.user)
      .subscribe(data => {
        if (this.handleToken(data)) {
          this.loading = false;
          this.router.navigate(['user/profile']);
        } else {
          this.loading = false;
          this.error = 'Sign Up error';
        }
      },
      error => {
        this.error = 'Sing Up error';
        this.loading = false;
      });
    }

    private handleToken(data: any): boolean {
      const access_token = data.token;
      const user = data.user;
      console.log(user);
      if (access_token && user) {
        localStorage.setItem(keys.TOKEN, JSON.stringify({
          token: access_token,
        }));
        localStorage.setItem(keys.USER, JSON.stringify({
          user: user
        }));
        console.log(localStorage);
        return true;
      } else {
        return false;
      }
    }
}
