import $ from 'jquery';
import { Component, OnInit } from '@angular/core';
import {AuthService} from '../../../services/auth.service';
import {Router} from '@angular/router';
import {keys} from '../../../constants/storage.keys';

@Component({
  selector: 'app/components-auth-login',
  templateUrl: './auth-login.component.html',
  styleUrls: ['./auth-login.component.scss'],
  providers: [AuthService]
})
export class AuthLoginComponent implements OnInit {

  username = '';
  password = '';
  error = '';
  params = {};
  loading = false;

  constructor(private router: Router, private authService: AuthService) { }

  ngOnInit() {
  }

  login() {
    this.loading = true;
    this.error = '';
    this.params = {
      username : this.username,
      password : this.password
    };
    this.authService.login(this.params)
          .subscribe(data => {
              if (this.handleToken(data)) {
                this.loading = false;
                this.router.navigate(['user/profile'])
              } else {
                this.error = 'Authentication error';
                this.loading = false;
              }
            },
            error => {
              this.error = 'Authentication error';
              this.loading = false;
            });
      }

    private handleToken(data: any): boolean {
      const access_token = data.token;
      const user = data.user;
      console.log(user);
      if (access_token && user) {
        localStorage.setItem(keys.TOKEN, JSON.stringify({
          token: access_token,
        }));
        localStorage.setItem(keys.USER, JSON.stringify({
          user: user
        }));
        console.log(localStorage);
        return true;
      } else {
        return false;
      }
    }
  }
