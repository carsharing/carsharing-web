import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'price-card',
  templateUrl: './price-card.component.html',
  styleUrls: ['./price-card.component.scss']
})
export class PriceCardComponent implements OnInit {
  @Input() rateCost: string;
  @Input() rateName: string;
  @Input() rateTiming: string;

  constructor() { }

  ngOnInit() {
  }

}
