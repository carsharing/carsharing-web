import { Component, OnInit } from '@angular/core';
import { TranslateService } from '@ngx-translate/core';

@Component({
  selector: 'app-footer',
  templateUrl: './footer.component.html',
  styleUrls: ['./footer.component.scss']
})
export class FooterComponent implements OnInit {

  currentLanguage = "";

  constructor(private translate: TranslateService) {
    this.currentLanguage = translate.store.defaultLang.toUpperCase();
  }

  ngOnInit() {
  }

  switchLanguage(language: string) {
    this.currentLanguage = language.toUpperCase();
    console.log(this.currentLanguage);
    this.translate.use(language);
  }


}
