import { Component, OnInit, ViewChild } from '@angular/core';
import { } from '@types/googlemaps';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  minuteRate = '0.1';
  kmRate = '0.3';
  hourRate = '12';
  dayRate = '60';
  rateTimingMinute = 'BYN/per minute';
  rateKm = 'BYN/per kilometre';
  rateTimingHour = 'BYN/per hour';
  rateTimingDay = 'BYN/per day'

  constructor() { }

  ngOnInit() {
    let mapProp = {
       center: new google.maps.LatLng(53.679182, 23.831749),
       zoom: 16,
       mapTypeId: google.maps.MapTypeId.ROADMAP
     };
   this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
   let marker = new google.maps.Marker({
            position: mapProp.center,
            map: this.map
          });
   }

   setMapType(mapTypeId: string) {
    this.map.setMapTypeId(mapTypeId)
  }

}
