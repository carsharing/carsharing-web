import { Component, OnInit } from '@angular/core';
import {UserService} from '../../services/user.service';
import {PassportService} from '../../services/passport.service';
import {keys} from '../../constants/storage.keys';

@Component({
  selector: 'app-profile-page',
  templateUrl: './profile-page.component.html',
  styleUrls: ['./profile-page.component.scss'],
  providers: [UserService]
})
export class ProfilePageComponent implements OnInit {

  userStatus = 'UserStatusActive';
  error = '';
  loading = false;

  user = {
    username: '',
    firstName: '',
    lastName: '',
    email: '',
    isActivated: false,
    roles: []
  }

  oldOrders = [
    {
      "mark" : "Opel",
      "model" : "Astra",
      "cost" : "30$",
      "time" : "30m"
    },
    {
      "mark" : "BMW",
      "model" : "520i",
      "cost" : "40$",
      "time" : "20m"
    }
  ];
  passport: any;

  constructor(private userService: UserService, private passportService: PassportService) {
    const user = this.userService.getUserInfoFromToken();
    this.user.username = user.username;
    this.user.firstName = '';
    this.user.lastName = '';
    this.user.email = user.email;
    this.user.isActivated = user.isActivated;
    this.user.roles = user.roles;
    if (user.passportId !== '') {
      this.getUserPassport(user.passportId);
    }
  }

  ngOnInit() {
  }

  getUserPassport(id: string) {
    this.passportService.getById(id)
      .subscribe((result) => {
        console.log(result);
        this.passport = result;
        console.log(this.passport);
        this.loading = false;
      },
      error => {
        this.error = 'Edit error';
        this.loading = false;
      });

  }
}
