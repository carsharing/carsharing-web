import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DashboardComponent } from './dashboard.component';
import { AdminAddCarComponent } from './dashboard-admin/content/admin-add-car/admin-add-car.component';
import { AdminCarsComponent } from './dashboard-admin/content/admin-cars/admin-cars.component';
import { AdminUsersComponent } from './dashboard-admin/content/admin-users/admin-users.component';
import { AdminUserDetailsComponent } from './dashboard-admin/content/admin-user-details/admin-user-details.component';
import { AdminCarDetailsComponent } from './dashboard-admin/content/admin-car-details/admin-car-details.component';
import { AdminMapComponent} from './dashboard-admin/content/admin-map/admin-map.component';
import { AdminMainComponent} from './dashboard-admin/content/admin-main/admin-main.component';

import { ProfilePageComponent } from '../profile-page/profile-page.component';
import { AuthUserDataComponent } from '../auth/auth-user-data/auth-user-data.component';
import { UserSupportComponent } from './dashboard-user/content/user-support/user-support.component';
import { ProfileEditComponent } from './dashboard-user/content/profile-edit/profile-edit.component';
import { UserGuideComponent } from './dashboard-user/content/user-support/user-guide/user-guide.component';
import { SettingsComponent } from './dashboard-user/content/settings/settings.component';
import { OrdersComponent } from './dashboard-user/content/orders/orders.component';
import { SettingsProfileComponent } from './dashboard-user/content/settings/content/settings-profile/settings-profile.component';

const routes: Routes = [
  { path: 'admin', component: DashboardComponent,
    children: [
      {
        path: 'main', component: AdminMainComponent
      },
      {
        path: 'cars', component: AdminCarsComponent
      },
      {
        path: 'add-car', component: AdminAddCarComponent
      },
      {
        path: 'users', component: AdminUsersComponent
      },
      {
        path: 'users/:id', component: AdminUserDetailsComponent
      },
      {
        path: 'cars/:id', component: AdminCarDetailsComponent
      },
      {
        path: 'map', component: AdminMapComponent
      },
    ]
  },
  {
    path: 'user', component: DashboardComponent, data: {breadcrumb: 'user'},
    children: [
      {
        path: 'profile', component: ProfilePageComponent,
          children: [
            {
              path: 'edit', component: ProfileEditComponent
            }
        ]
      },
      {
        path: 'orders', component: OrdersComponent
      },
      {
        path: 'userdata', component: AuthUserDataComponent
      },
      {
        path:'support', component: UserSupportComponent,
          children: [
            {
              path: 'guide', component: UserGuideComponent
            }
          ]
      },
      {
        path: 'settings', component: SettingsComponent, data: {breadcrumb: 'settings'},
          children: [
            {
              path: 'profile', component: SettingsProfileComponent
            }
          ]
      },
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
