import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardComponent } from './dashboard.component';
import { DashboardRoutingModule } from './dashboard-routing.module';

import { AdminMainModule} from './dashboard-admin/content/admin-main/admin-main.module';
import { AdminAddCarModule} from './dashboard-admin/content/admin-add-car/admin-add-car.module';
import { AdminMapModule} from './dashboard-admin/content/admin-map/admin-map.module';
import { AdminUsersModule} from './dashboard-admin/content/admin-users/admin-users.module';
import { AdminUserDetailsModule} from './dashboard-admin/content/admin-user-details/admin-user-details.module';
import { AdminCarsModule} from './dashboard-admin/content/admin-cars/admin-cars.module';
import { AdminCarDetailsModule} from './dashboard-admin/content/admin-car-details/admin-car-details.module';
import { SidebarModule} from '../common/sidebar/sidebar.module';
import { DashboardNavbarModule } from './dashboard-navbar/dashboard-navbar.module';

import { UserSupportModule } from './dashboard-user/content/user-support/user-support.module';
import { SettingsModule } from './dashboard-user/content/settings/settings.module';
import { OrdersModule } from './dashboard-user/content/orders/orders.module';
import { BreadcrumbComponent } from '../common/breadcrumb/breadcrumb.component';

import { AuthUserDataModule } from '../auth/auth-user-data/auth-user-data.module';
import { ProfileEditModule } from './dashboard-user/content/profile-edit/profile-edit.module';

@NgModule({
  imports: [
    CommonModule,
    UserSupportModule,
    AdminMainModule,
    AdminAddCarModule,
    AdminUsersModule,
    AdminUserDetailsModule,
    AdminCarsModule,
    SidebarModule,
    SettingsModule,
    AdminCarDetailsModule,
    DashboardNavbarModule,
    DashboardRoutingModule,
    AdminMapModule,
    AuthUserDataModule,
    ProfileEditModule,
    OrdersModule
  ],
  declarations: [
    DashboardComponent,
    BreadcrumbComponent
 ]
})
export class DashboardModule { }
