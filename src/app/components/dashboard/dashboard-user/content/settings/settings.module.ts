import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { NgbModule } from '@ng-bootstrap/ng-bootstrap';
import { FormsModule } from '@angular/forms';
import { SettingsComponent } from './settings.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { SettingsPassportComponent } from './content/settings-passport/settings-passport.component';
import { SettingsProfileComponent } from './content/settings-profile/settings-profile.component';

@NgModule({
  imports: [
    CommonModule,
    NgbModule,
    FormsModule,
    RouterModule,
    SharedModule,
  ],
  declarations: [
    SettingsComponent,
    SettingsPassportComponent,
    SettingsProfileComponent,
  ]
})
export class SettingsModule { }
