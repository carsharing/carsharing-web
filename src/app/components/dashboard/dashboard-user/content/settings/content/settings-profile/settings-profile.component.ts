import { Component, OnInit } from '@angular/core';
import {UserService} from '../../../../../../../services/user.service';
import {keys} from '../../../../../../../constants/storage.keys';

@Component({
  selector: 'app-settings-profile',
  templateUrl: './settings-profile.component.html',
  styleUrls: ['./settings-profile.component.scss']
})
export class SettingsProfileComponent implements OnInit {

  keks =  {
    keks: 'des'
  }
  user = {
    _id: '',
    username: '',
    email: '',
    skype: '',
    linkedin: '',
    phoneNumber: '',
    bio: ''
  };
  loading = false;
  error = '';

  constructor(private userService: UserService) {
    this.getUserInfo();
  }

  ngOnInit() {
  }

  getUserInfo() {
    this.user =  this.userService.getUserInfoFromToken();
  }

  editUserProfile() {
    let newUserParams = {
      _id: this.user._id,
      username: this.user.username,
      email: this.user.email,
      skype: this.user.skype,
      linkedin: this.user.linkedin,
      phoneNumber: this.user.phoneNumber,
      bio: this.user.bio,
    }
    this.userService.editProfile(newUserParams)
      .subscribe(() => {

        this.handleToken(newUserParams);
        console.log(localStorage.getItem(keys.USER));
        this.loading = false;
        },
        error => {
          this.error = 'Edit error';
          this.loading = false;
          const user =  JSON.parse(localStorage.getItem('USER')).user;
          this.user._id = user._id;
          this.user.username = user.username;
          this.user.email = user.email;
          this.user.skype = user.skype;
          this.user.linkedin = user.linkedin;
          this.user.phoneNumber = user.phoneNumber;
          this.user.bio = user.bio;
        });
    }

    private handleToken(user: any): boolean {
      if (user) {
        localStorage.setItem(keys.USER, JSON.stringify({
          user: user
        }));
        return true;
      } else {
        return false;
      }
    }

}
