import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SettingsPassportComponent } from './settings-passport.component';

describe('SettingsPassportComponent', () => {
  let component: SettingsPassportComponent;
  let fixture: ComponentFixture<SettingsPassportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SettingsPassportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SettingsPassportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
