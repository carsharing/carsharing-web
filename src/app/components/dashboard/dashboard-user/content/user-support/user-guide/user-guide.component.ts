import { Component, OnInit } from '@angular/core';
import { AlertService } from '../../../../../../services/alert.service';

@Component({
  selector: 'app-user-guide',
  templateUrl: './user-guide.component.html',
  styleUrls: ['./user-guide.component.scss']
})
export class UserGuideComponent implements OnInit {

  constructor(private alertService: AlertService) { }

  ngOnInit() {
  }

  success(message: string) {
     this.alertService.success(message);
  }

  error(message: string) {
     this.alertService.error(message);
  }

  info(message: string) {
     this.alertService.info(message);
  }

  warn(message: string) {
     this.alertService.warn(message);
  }

  clear() {
     this.alertService.clear();
  }
}
