import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { UserSupportComponent } from './user-support.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { SupportCardComponent } from './support-card/support-card.component';
import { RouterModule } from '@angular/router';
import { UserGuideComponent } from './user-guide/user-guide.component';

@NgModule({
  imports: [
    CommonModule,
    RouterModule,
    SharedModule
  ],
  declarations: [
    UserSupportComponent,
    UserGuideComponent,
    SupportCardComponent
  ]
})
export class UserSupportModule { }
