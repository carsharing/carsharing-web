import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { DataTableResource } from 'angular5-data-table';
import {UserService} from '../../../../../services/user.service';

import persons from './users-mock-data';

@Component({
  selector: 'app-admin-users',
  templateUrl: './admin-users.component.html',
  styleUrls: ['./admin-users.component.scss']
})
export class AdminUsersComponent implements OnInit {

  itemResource: any; // = new DataTableResource(persons);
  items = [];
  itemCount = 0;
  loading = false;
  error = '';

  constructor(private router: Router, private userService: UserService) {

  }

  ngOnInit() {
    this.userService.getUsersList()
    .subscribe((result) => {
        this.loading = false;
        this.itemResource = new DataTableResource(result);
        console.log(this.itemResource);
        this.itemResource.count().then(count => this.itemCount = count);
      },
      error => {
        this.error = 'Edit error';
        this.loading = false;
      });
  }

  reloadItems(params) {
    if(this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  // special properties:
  rowClick(rowEvent) {
      console.log('Clicked: ' + rowEvent.row.item.name);
  }

  rowDoubleClick(rowEvent) {
    this.router.navigate(['/admin/users/'+rowEvent.row.item._id])
  }

  rowTooltip(item) { return item.jobTitle; }


}
