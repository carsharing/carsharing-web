import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminUsersComponent } from './admin-users.component';
import { DataTableModule } from 'angular5-data-table';
import {UserService} from '../../../../../services/user.service';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule
  ],
  providers: [UserService],
  declarations: [AdminUsersComponent]
})
export class AdminUsersModule { }
