import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {CarService} from '../../../../../services/car.service';

@Component({
  selector: 'app-admin-car-details',
  templateUrl: './admin-car-details.component.html',
  styleUrls: ['./admin-car-details.component.scss']
})
export class AdminCarDetailsComponent implements OnInit {

  router: any;
  carId: any;
  loading = false;
  error = '';
  car: any;

  constructor(private route: ActivatedRoute, private carService: CarService) {
    this.route.params.subscribe( params => { console.log(params);this.carId = params.id } );

  }

  ngOnInit() {
    this.carService.getCarById(this.carId)
      .subscribe((result) => {
        this.loading = false;
        console.log(result);
        this.car = result;
      },
      error => {
        this.error = 'Get by Id error';
        this.loading = false;
      });
  }

}
