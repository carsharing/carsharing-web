import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminCarDetailsComponent } from './admin-car-details.component';
import { SharedModule } from '../../../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [AdminCarDetailsComponent]
})
export class AdminCarDetailsModule { }
