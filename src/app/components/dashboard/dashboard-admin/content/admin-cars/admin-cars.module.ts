import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminCarsComponent } from './admin-cars.component';
import { DataTableModule } from 'angular5-data-table';
import { RouterModule } from '@angular/router';

@NgModule({
  imports: [
    CommonModule,
    DataTableModule,
    RouterModule
  ],
  declarations: [AdminCarsComponent],
  exports: [
    AdminCarsComponent
  ]
})
export class AdminCarsModule { }
