import { Component, OnInit } from '@angular/core';
import {Router} from '@angular/router';

import { DataTableResource } from 'angular5-data-table';
import {CarService} from '../../../../../services/car.service';

import cars from './cars-mock-data';

@Component({
  selector: 'app-admin-cars',
  templateUrl: './admin-cars.component.html',
  styleUrls: ['./admin-cars.component.scss']
})
export class AdminCarsComponent implements OnInit {

  itemResource: any //= new DataTableResource(cars);
  items = [];
  itemCount = 0;
  loading = false;
  error = '';

  constructor(private router: Router, private carService: CarService) {
  }

  ngOnInit() {
    this.carService.getCarsList()
    .subscribe((result) => {
      this.loading = false;
      console.log(result);
      this.itemResource = new DataTableResource(result);
      console.log(this.itemResource);
      this.itemResource.count().then(count => this.itemCount = count);
    },
    error => {
      this.error = 'Edit error';
      this.loading = false;
    });
  }

  reloadItems(params) {
    if(this.itemResource) {
      this.itemResource.query(params).then(items => this.items = items);
    }
  }

  // special properties:
  rowClick(rowEvent) {
      console.log('Clicked: ' + rowEvent.row.item.name);
  }

  rowDoubleClick(rowEvent) {
    console.log(rowEvent.row.item._id);
    this.router.navigate(['/admin/cars/'+rowEvent.row.item._id])

  }

  rowTooltip(item) { return item.jobTitle; }
}
