import { Component, OnInit } from '@angular/core';
import {NgForm} from '@angular/forms';

import {CarService} from '../../../../../services/car.service';
import {Car} from '../../../../../models/car';


@Component({
  selector: 'app-admin-add-car',
  templateUrl: './admin-add-car.component.html',
  styleUrls: ['./admin-add-car.component.scss'],
  providers: [CarService]
})
export class AdminAddCarComponent implements OnInit {

  bodyTypesList = [ 'Microcar', 'Subcompact car', 'Compact car' ];
  gearBoxTypesList = ['Automatic', 'Manual'];
  fuelTypesList = ['Diesel', 'Petrol', 'Hybrid (petrol/electric)', 'Electric'];

  selectedFiles = null;
  error = '';
  loading = false;

  car: Car;

  constructor(private carService: CarService) { }

  ngOnInit() {
    this.car = new Car('', '', '', 0, '', '', 0, 0, '', 0, 0);
  }

  onFileSelected(e) {
    this.selectedFiles = e.target.files;
  }

  onAddingCar() {
    this.carService.addingCar(this.car)
    .subscribe(() => {
      alert('car was added');
      this.loading = false;
      },
      error => {
        this.error = 'Edit error';
        this.loading = false;
      });
  }

}
