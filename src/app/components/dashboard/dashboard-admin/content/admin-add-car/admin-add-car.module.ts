import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import {NgForm} from '@angular/forms';

import { CarService } from '../../../../../services/car.service';
import { AdminAddCarComponent } from './admin-add-car.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule
    ],
    providers: [
      CarService
    ],
  declarations: [AdminAddCarComponent]
})
export class AdminAddCarModule { }
