import $ from 'jquery';
import { Component, OnInit, ViewChild, ComponentFactoryResolver, ComponentFactory, ComponentRef, ApplicationRef, } from '@angular/core';
import {Router} from '@angular/router';
import { } from '@types/googlemaps';

import {CarService} from '../../../../../services/car.service';
import {OrderService} from '../../../../../services/order.service';
import {UserService} from '../../../../../services/user.service';

@Component({
  selector: 'app-admin-map',
  templateUrl: './admin-map.component.html',
  styleUrls: ['./admin-map.component.scss']
})
export class AdminMapComponent implements OnInit {

  @ViewChild('gmap') gmapElement: any;
  map: google.maps.Map;

  carsList: any;
  carCoordinats = [
    {
      position: {
        lat: 53.683178,
        lng: 23.830939
      },
    },
    {
      position: {
        lat: 53.679182,
        lng: 23.842611
      },
    },
    {
      position: {
        lat: 53.685453,
        lng: 23.830950
      },
    },
    {
      position: {
        lat: 53.678579,
        lng: 23.830234
      },
    },
    {
      position: {
        lat: 53.678964,
        lng: 23.831099
      },
    },
    {
      position: {
        lat: 53.683024,
        lng: 23.835445
      },
    },
    {
      position: {
        lat: 53.681464,
        lng: 23.827588
      },
    },
    {
      position: {
        lat: 53.678204,
        lng: 23.826260
      },
    }
  ];

  image = {
    url: '/assets/img/smart-car.png',
    size: new google.maps.Size(64, 64),
    // The origin for this image is (0, 0).
    origin: new google.maps.Point(0, 0),
    // The anchor for this image is the base of the flagpole at (0, 32).
    anchor: new google.maps.Point(0, 32)
  }
  carsCount: number;
  loading = false;
  error = '';
  order: any;

  constructor(private carService: CarService, private userService: UserService,
      private orderService: OrderService, private router: Router) {
        this.getCars();
        setInterval(()=> {
          this.getCars();
        }, 5000);
  }

  ngOnInit() {
    let mapProp = {
      center: new google.maps.LatLng(53.679182, 23.831749),
      zoom: 15,
      mapTypeId: google.maps.MapTypeId.ROADMAP
    };
    this.map = new google.maps.Map(this.gmapElement.nativeElement, mapProp);
  }

  async getCars() {
      await this.carService.getCarsList()
      .subscribe((result) => {
        this.loading = false;

        this.carsList = result;
        this.getCarsCount();

        for(let i = 0; i < this.carsList.length; i++) {
          this.carsList[i].position = this.carCoordinats[i].position;
        }

        // Markers
        for (let i = 0; i < this.carCoordinats.length; i++) {
          this.addMarker(this.carsList[i]);
        };
      },
      error => {
        this.error = 'Edit error';
        this.loading = false;
      });

  }

  setMapType(mapTypeId: string) {
    this.map.setMapTypeId(mapTypeId)
  }

  addMarker(param: any) {
    if (param === undefined) return false;
    let marker = new google.maps.Marker({
      position: param.position,
      map: this.map,
      icon: this.image
    })

    let content = '<div class="popup" style="background-color: #fff; border-radius: 10px;">'+
        '<div class="popup__img-container" style="display: flex;justify-content: space-around;height: 95px;">'+
          '<img class="popup__img" style="width: 150px;height: 80px;top: 0;position: relative;" src="https://www.masmotors.ru/colors/volkswagen-polo/4.png" >'+
        '</div>'+
      '<div class="popup__body-container">'+
        '<div class="popup__body">'+
          '<div class="popup__title-container">'+
            '<h5 class="popup__title " style="margin:0;font-weight: bolder" translate>Mark and model</h5>'+
            '<span class="popup__title-text" style="font-size: 14px;font-weight: bold;">'+
            param.mark+' '+param.model+
            '</span>'+
          '</div>'+
          '<div class="popup__title-container">'+
            '<h5 class="popup__title" style="margin:0;font-weight: bolder" translate>Fuel gauge</h5>'+
            '<span class="popup__title-text" style="font-size: 14px;font-weight: bold;color:green;">~375km</span>'+
          '</div>'+
          '<div class="popup__title-container">'+
            '<h5 class="popup__title" style="margin:0;font-weight: bolder" translate>Side number</h5>'+
            '<span class="popup__title-text" style="font-size: 14px;font-weight: bold;">'+
            param.sideNumber +
            '</span>'+
          '</div>'+
          '<div class="popup__title-container">'+
            '<h5 class="popup__title" style="margin:0;font-weight: bolder" translate>Distance to car</h5>'+
            '<span class="popup__title-text" style="font-size: 14px;font-weight: bold;">5 minute</span>'+
          '</div>'+
          '<div class="popup__title-container">'+
            '<h5 class="popup__title" style="margin:0;font-weight: bolder;" translate>Price</h5>'+
            '<span class="popup__title-text" style="font-size: 14px;font-weight: bold;">0.15 $ per/minute</span>'+
          '</div>'+
          '<div class="popup__title-container popup__btn-container" style="display: flex; justify-content: center; margin-top: 10px;">'+
            '<button id="infoWindowBtn" class="popup__btn" type="button" class="popup__title" style="padding: 5px 30px; background: #3acd31; border: 1px solid #a0b5b5; cursor: pointer; margin:0;font-weight: bolder;" >Order</button>'+
          '</div>'+
        '</div>'+
      '</div>'+
    '</div>';

    let infoWindow = new google.maps.InfoWindow({
      content: content
    });

    infoWindow.addListener('domready', () => {
      document.getElementById("infoWindowBtn").addEventListener("click", () => {
          let carId = param._id;
          let userId = this.userService.getUserInfoFromToken()._id;
          console.log(carId, userId);
          this.orderService.addingOrder(userId, carId)
            .subscribe((result) => {
              this.loading = false;
              this.order = result;
              console.log(this.order);

            },
            error => {
              this.error = 'Edit error';
              this.loading = false;
            });

          // this.router.navigate(['/user/order/'+ carId]);
      });
    });

    marker.addListener('click', () => {
      infoWindow.open(this.map, marker)
    });
  }

  getCarsCount() {
    if(this.carsList) this.carsCount = this.carsList.length;
  }
}
