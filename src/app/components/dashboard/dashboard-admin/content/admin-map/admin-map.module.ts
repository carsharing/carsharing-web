import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminMapComponent } from './admin-map.component';
import { SharedModule } from '../../../../../shared/shared.module';
import { OrderService } from '../../../../../services/order.service';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  providers: [OrderService],
  declarations: [
    AdminMapComponent
  ]
})
export class AdminMapModule { }
