import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AdminUserDetailsComponent } from './admin-user-details.component';
import { SharedModule } from '../../../../../shared/shared.module';

@NgModule({
  imports: [
    CommonModule,
    SharedModule
  ],
  declarations: [AdminUserDetailsComponent]
})
export class AdminUserDetailsModule { }
