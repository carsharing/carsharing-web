import { Component, OnInit } from '@angular/core';
import {ActivatedRoute} from "@angular/router";
import {UserService} from '../../../../../services/user.service';
import {PassportService} from '../../../../../services/passport.service';

@Component({
  selector: 'app-admin-user-details',
  templateUrl: './admin-user-details.component.html',
  styleUrls: ['./admin-user-details.component.scss']
})
export class AdminUserDetailsComponent implements OnInit {

  router: any;
  userId: any;
  loading = false;
  error = '';
  user: any;
  passport: any;

  constructor(private route: ActivatedRoute, private userService: UserService, private passportService: PassportService) {
    this.route.params.subscribe( params => { this.userId = params.id } );
  }

  ngOnInit() {
    if(this.userId) {
      this.getUser();
    }
  }

  getUser() {
    this.userService.getById(this.userId)
      .subscribe((result) => {
        this.loading = false;
        this.user = result;
        console.log(result);
        if(this.user.passportId) this.getPassport();
      },
      error => {
        this.error = 'Get by Id error';
        this.loading = false;
      });
  }

  getPassport() {
    console.log(this.user.passportId);
    this.passportService.getById(this.user.passportId)
    .subscribe((result) => {
      this.loading = false;
      console.log(result);
      this.passport = result;
    },
    error => {
      this.error = 'Get by Id error';
      this.loading = false;
    });
  }

}
