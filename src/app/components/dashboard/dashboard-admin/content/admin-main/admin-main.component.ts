import { Component, OnInit } from '@angular/core';
import { Chart } from 'chart.js';
import { StatisticService } from '../../../../../services/statistic.service';
import {OrderService} from '../../../../../services/order.service';

@Component({
  selector: 'app-admin-main',
  templateUrl: './admin-main.component.html',
  styleUrls: ['./admin-main.component.scss']
})
export class AdminMainComponent implements OnInit {

  chart = []; // This will hold our chart info

  usersCount = 320;
  notActivatedUsers = 10;
  usersText = 'Users need to be activated';
  carsCount = 30;
  rentedCars = 10;
  carsText = 'You need to check cars';
  messagesCount = 30;

  loading = false;
  error = '';
  supportCount = 10;
  ordersList: any;
  constructor(private statisticService: StatisticService, private orderService: OrderService) {
      this.getOrdersList();
  }

  ngOnInit() {

  }

  async getOrdersList() {
    this.loading = true;
    await this.orderService.list()
      .subscribe((result) => {
        console.log(result);
        this.ordersList = result;
        console.log(this.ordersList);
        this.loading = false;
      },
      error => {
        this.error = 'Edit error';
        this.loading = false;
      });
  }
}
