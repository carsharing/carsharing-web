import { Component, OnInit } from '@angular/core';
import { SidebarService } from '../../../services/sidebar.service';

@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss'],
  providers: [SidebarService]
})
export class SidebarComponent implements OnInit {

  isExpanded: boolean;

  constructor(private sidebarService: SidebarService) {
    this.isExpanded = this.sidebarService.isExpanded;
  }

  ngOnInit() {
  }

  toggleSidebar(e) {
     this.isExpanded = this.sidebarService.toggleSidebar();
  }

}
