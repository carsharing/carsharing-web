import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SidebarComponent } from './sidebar.component';
import { RouterModule } from '@angular/router';
import { SidebarService } from '../../../services/sidebar.service';

@NgModule({
  imports: [
    CommonModule,
    RouterModule
  ],
  declarations: [SidebarComponent],
  exports: [SidebarComponent],
  providers: [
    SidebarService
  ]
})
export class SidebarModule { }
